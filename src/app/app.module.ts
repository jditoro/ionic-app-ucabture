import { HTTP } from '@ionic-native/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import {GoogleMaps} from '@ionic-native/google-maps';
import { FeedbackPage } from '../pages/feeedback/feedback';
import { homedir } from 'os';
import { ModalComponent } from '../components/modal/modal';
import { ComponentsModule } from '../components/components.module';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { HttpModule } from '@angular/http';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp,
    FeedbackPage,
    LoginPage,
    HomePage,
    RegisterPage,
    ForgotPasswordPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FeedbackPage,
    ModalComponent,
    LoginPage,
    HomePage,
    RegisterPage,
    ForgotPasswordPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    FileTransfer,
    File,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Camera,
    Geolocation,
    GoogleMaps
  ]
})
export class AppModule { }
