import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Events } from "ionic-angular";
import { HomePage } from "../home/home";
import {Geolocation} from '@ionic-native/geolocation';

// @IonicPage()
@Component({
    selector: 'page-feedback',
    templateUrl: 'feedback.html'
})

export class FeedbackPage {
    private feel: number = null;
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public event: Events,
                public geolocation: Geolocation){
                    let imgData = navParams.get('img');
                }
    
    pickEmoji(emojiNum) {
        if(this.feel == null) {
            showCommentBox();
        }
        this.feel = emojiNum;
    }

    close() {
        this.navCtrl.pop();
    }

    sendInfo() {
        // Here we catch all the info, and send it to backend
        let form : HTMLFormElement = document.querySelector('#feels') as HTMLFormElement;
        let comment : HTMLFormElement = document.querySelector('#comment-box') as HTMLFormElement;
        let feel = this.feel;

        console.log(comment.value, this.feel, feel);
        this.geolocation.getCurrentPosition().then((resp) => {
            this.event.publish('marker:create', {
                emoji: feel,
                comment: comment.value,
                location: {
                    lat: resp.coords.latitude,
                    lng: resp.coords.longitude
                }
            });
           }).catch((error) => {
             console.log('Error getting location', error);
           });
           
        this.feel = null;
        
        form.reset();
        this.navCtrl.popToRoot();
        hideCommentBox();
    }

    
}
    function showCommentBox(){
        let sendBtn: HTMLElement = document.querySelector('.send-btn') as HTMLElement;
        let commentBox: HTMLElement = document.querySelector('#comment-box') as HTMLElement;
        let container: HTMLElement = document.querySelector('.opinion-container') as HTMLElement
        
        container.style.transform = 'translateY(-110%)';
        commentBox.style.transform = 'translateY(0px)';
        sendBtn.style.transform = 'translateY(0px)';
    }

    function hideCommentBox(){
        let sendBtn: HTMLElement = document.querySelector('.send-btn') as HTMLElement;
        let commentBox: HTMLElement = document.querySelector('#comment-box') as HTMLElement;
        let container: HTMLElement = document.querySelector('.opinion-container') as HTMLElement
        
        container.style.transform = null;
        commentBox.style.transform = null;
        sendBtn.style.transform = null;
    }