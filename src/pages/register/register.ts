import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  req: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

  sendInfo(name, lastname, username, password, email) {

    console.log('android', this.platform.is('android'))
    console.log('cordova', this.platform.is('cordova'))
    console.log('core', this.platform.is('core'))
    console.log('mobile', this.platform.is('mobile'))

    console.log('campos =', name, lastname, username, password, email);


    const url = 'https://ucabture.herokuapp.com/signup'

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let body = {
      name: name,
      lastname: lastname,
      username: username,
      password: password,
      email: email
    };

    // Hace la peticion dependiendo si nos ejecutamos en mobil o en desktop
    if (this.platform.is('android') || this.platform.is('cordova') || this.platform.is('mobile')) {
      this.req = new HTTP();
      this.req.post(url, body, headers).then(data => {
        console.log('EXITO');
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);
        this.navCtrl.push(HomePage);
      }).catch(error => {
        console.log('ERROR');
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
      })
    } else if (this.platform.is('core')) {
      this.http.post(url, JSON.stringify(body), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(`data es ${data.status}`);

          if (data.status === 'OK') {
            console.log('Puede pasar');
            this.navCtrl.setRoot(HomePage);
          }
        });
    }

  }

}
