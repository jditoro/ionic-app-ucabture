import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  req: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goHome(username, password) {
    console.log(username, password);

    const url = 'https://ucabture.herokuapp.com/login'
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let body = {
      username: username,
      password: password,
    };

    // Hace la peticion dependiendo si nos ejecutamos en mobil o en desktop
    if (this.platform.is('android') || this.platform.is('cordova') || this.platform.is('mobile')) {
      this.req = new HTTP();
      this.req.post(url, body, headers).then(data => {
        console.log('EXITO');
        console.log(data.status);
        console.log(data.data); // data received by server
        console.log(data.headers);

        if (data.status === 200) {
          this.navCtrl.push(HomePage);
        } else {
          console.log("No puede pasar");
        }
      }).catch(error => {
        console.log('ERROR');
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
      })
    } else if (this.platform.is('core')) {
      this.http.post(url, JSON.stringify(body), { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          console.log(`data es ${data}`);

          if (data.status === 'OK') {
            console.log('Puede pasar');
            this.navCtrl.setRoot(HomePage);
          } else {
            console.log('No puede pasar');
          }
        });
    }



    // this.navCtrl.goToRoot({});
  }

  goToRegister() {
    this.navCtrl.push(RegisterPage);
  }
  
  goToForgot() {
    this.navCtrl.push(ForgotPasswordPage);
  }
}
