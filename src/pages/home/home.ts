import { Component, ViewChild, ElementRef } from "@angular/core";
import { IonicPage, NavController, ModalController, Events } from "ionic-angular";
import { Http } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Geolocation } from "@ionic-native/geolocation";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { FeedbackPage } from "../feeedback/feedback";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
// import { Camera, CameraOptions } from "../../../node_modules/@ionic-native/camera";
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  CameraPosition,
  MarkerOptions,
  Marker
} from '@ionic-native/google-maps';
import { ModalComponent } from "../../components/modal/modal";

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  @ViewChild("map")
  mapElement: ElementRef;



  // map: GoogleMap;
  map: any;
  photo: any;
  modal: any;
  icons: any;
  req: any;

  constructor(
    public navCtrl: NavController,
    public geolocation: Geolocation,
    public camera: Camera,
    private modalCtrl: ModalController,
    public events: Events,
    public http: Http,
    public platform: Platform,
    private transfer: FileTransfer, private file: File
  ) {
    this.events.subscribe('marker:create', this.addMarker.bind(this));
  }

  launchCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let base64Image = "data:image/jpeg;base64," + imageData;
        this.photo = base64Image;
        console.log('listo');
      },
      err => {
        // Handle error
      }
    );
    this.navCtrl.push(FeedbackPage, { img: this.photo });
  }

  getIcon(index: number) {
    const icons = [
      {
        url: "assets/markers/marker1.png",
        scaledSize: new google.maps.Size(64, 64)
      },
      {
        url: "assets/markers/marker2.png",
        scaledSize: new google.maps.Size(64, 64)
      },
      {
        url: "assets/markers/marker3.png",
        scaledSize: new google.maps.Size(64, 64)
      },
      {
        url: "assets/markers/marker4.png",
        scaledSize: new google.maps.Size(64, 64)
      },
      {
        url: "assets/markers/marker5.png",
        scaledSize: new google.maps.Size(64, 64)
      },
      {
        url: "assets/markers/marker6.png",
        scaledSize: new google.maps.Size(64, 64)
      }
    ];

    return icons[index - 1];
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  // getActualLocation() {
  //   this.geolocation.getCurrentPosition().then((position) => {

  //   })
  // }

  loadMap() {
    let mapEle: HTMLElement = document.getElementById('map');
    this.map = new google.maps.Map(mapEle, {
      zoom: 20,
      center: new google.maps.LatLng(8.29674, -62.711653)
    });

    const url = 'https://ucabture.herokuapp.com/leo1/feed'

    // Hace la peticion dependiendo si nos ejecutamos en mobil o en desktop
    if (this.platform.is('android') || this.platform.is('cordova') || this.platform.is('mobile')) {
      this.req = new HTTP();
      this.req.get(url, {}, {})
        .then(res => {
          console.log(res.status);
          console.log(res.data); // data received by server
          let images = JSON.parse(res.data).images;
          let iconFn = this.getIcon;
          let originalMap = this.map;
          images.forEach(function (img) {
            var marker = new google.maps.Marker({
              position: new google.maps.LatLng(img.lat, img.lng),
              icon: iconFn(img.emoji),
              map: originalMap,
            });
          })
        })

    } else if (this.platform.is('core')) {

    }


    // let mapOptions: any = {
    //   camera: {
    //     target: {
    //       lat: 8.29674, // default location
    //       lng: -62.711653 // default location
    //     },
    //     zoom: 18,
    //     tilt: 30
    //   }
    // };

    // this.map = GoogleMaps.create('map_canvas', mapOptions);

    // // Wait the MAP_READY before using any methods.
    // this.map.one(GoogleMapsEvent.MAP_READY)
    //   .then(() => {
    //     // Now you can use all methods safely.
    //     // this.getPosition();
    //   })
    //   .catch(error => {
    //     console.log(error);
    //   });

    // this.geolocation.getCurrentPosition().then(
    //   position => {
    //     let latLng = new google.maps.LatLng(
    //       position.coords.latitude,
    //       position.coords.longitude
    //     );

    //     let mapOptions = {
    //       center: latLng,
    //       zoom: 17,
    //       mapTypeId: google.maps.MapTypeId.ROADMAP,
    //       disableDefaultUI: true
    //     };

    //     this.map = new google.maps.Map(
    //       this.mapElement.nativeElement,
    //       mapOptions
    //     );

    //     const res = {
    //       images: [
    //         {
    //           description: "Primera imagen",
    //           emoji: 3,
    //           lat: 8.296317,
    //           lng: -62.71194,
    //           url: "http://localhost:4200/assets/marker3.png"
    //         },
    //         {
    //           description: "Segunda imagen",
    //           emoji: 4,
    //           lat: 8.296869,
    //           lng: -62.711677,
    //           url: "http://localhost:4200/assets/marker4.png"
    //         },
    //         {
    //           description: "Tercera imagen",
    //           emoji: 2,
    //           lat: 8.296693,
    //           lng: -62.711554,
    //           url: "http://localhost:4200/assets/marker2.png"
    //         },
    //         {
    //           description: "blah blah blah",
    //           emoji: 1,
    //           lat: 8.296713,
    //           lng: -62.711375,
    //           url: "http://localhost:4200/assets/marker1.png"
    //         }
    //       ]
    //     };

    //     const images = res.images;
    //     let that = this;
    //     images.forEach(function(img) {
    //       var marker = new google.maps.Marker({
    //         position: new google.maps.LatLng(img.lat, img.lng),
    //         icon: that.getIcon(img.emoji),
    //         map: that.map
    //       });

    //       marker.addListener("click", () => {
    //         // console.log('asdasdas');
    //         that.modal = that.modalCtrl.create(ModalComponent, {
    //           emoji: img.emoji,
    //           comment:
    //             "Increible universidad, me encanta como se ve la casa del estudiante."
    //         });
    //         that.modal.present();
    //       });
    //     });
    //   },
    //   err => {
    //     console.log(err);
    //   }
    // );
  }

  addMarker(args: any) {
    console.log(args);
    let that = this;
    console.log(that);
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(args.location.lat, args.location.lng),
      icon: that.getIcon(args.emoji),
      map: that.map
    });

    marker.addListener("click", () => {
      that.modal = that.modalCtrl.create(ModalComponent, {
        emoji: args.emoji,
        comment: args.comment,
        img: args.img
      });
      that.modal.present();
    },
      err => {
        console.log(err);
      }
    );
  }
}
