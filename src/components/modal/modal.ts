import { Component, Input } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'modal',
  templateUrl: 'modal.html'
})
export class ModalComponent {

  text : string;
  comment : string;
  icon : string;

  constructor(public viewCtrl: ViewController, public params : NavParams) {
    console.log('icon ', this.icon);
    this.text = 'Hello World';
    this.icon = this.params.get('emoji');
    this.comment = this.params.get('comment');
    this.findEmoji();
  }

  findEmoji(){
    this.icon = '/assets/emojis/' + this.icon +'.png';
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
